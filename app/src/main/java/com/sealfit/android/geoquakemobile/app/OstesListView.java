/**
 * Created by jgarcia on 6/04/15.
 * ListView to display seismic events.
 * This is used to display, to the user
 * a list of seismic events.
 *
 * Language: Android/Java.
 *
 */



package com.sealfit.android.geoquakemobile.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.ListView;
import android.widget.Toast;

public class OstesListView extends ListFragment {


    /**
     *  String used to identify
     */
    private static final String ListView = "ListView";

    /**
     * Android reference to config information.
     */
    private SharedPreferences mListViewPrefs;


    private int mUrlResourceId;

    /**
     * Url to the USGS.gov data source.
     */
    private String mUrlName;

    private EarthQuakeEvent mEarthQuakeEventInfo;

    /**
     * WebView used to display USGS data about the seismic event.
     * This merely opens a browser window widget and displays the USGS
     * html page pertaining to the event.
     */
    private WebView mWebView;

    /**
     * Java weak reference used to handle
     * data within an inner class.
     */
    private WeakReference<GetGeoJsonData>asyncTaskWeakRef;


    /**
     * used to create a class instance.
     * @return
     */
     public static OstesListView newInstance(){

        return new OstesListView();

    }


    /**
     * User cannot instantiate a class
     * as it is private.
     */
    private OstesListView(){


    }


    /**
     * *onCreate method.  Any initialization, before view creation will take place here.
     */
    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);




         /**
          *
          * load a toast message, notifying the user what
          * data setting they are currently using.
          *
          */
        mListViewPrefs = getActivity().getSharedPreferences("com.example.sealfit.android.geoquakemobile.prefs", Context.MODE_PRIVATE);


        mUrlResourceId = mListViewPrefs.getInt("radio_string", 0);
        mUrlName = getActivity().getResources().getString(mUrlResourceId);

        if(mUrlName != null){

            Toast.makeText(getActivity(), "Showing all events past " + mUrlName , Toast.LENGTH_SHORT).show();

        }
        this.startNewAsyncTask();



    }

    /**
     * Execute the async task to get the GeoJson Data.
     *
     */
    private void startNewAsyncTask(){

        GetGeoJsonData asyncTask = new GetGeoJsonData();
        this.asyncTaskWeakRef = new WeakReference<GetGeoJsonData>(asyncTask);
        asyncTask.execute(mUrlName);

    }


    // Just use the default ListView layout...
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        this.setRetainInstance(true);




        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     *
     * When the user clicks on the list item.  We
     * need to bring up more information.
     */

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){


        // Used for debuggin only....
        Log.d(ListView, "Item Clicked: " + id);
        Log.d(ListView, "Item Position: " + position);
        Log.d(ListView, "List Count: " + this.getListView().getAdapter().getCount());
        mEarthQuakeEventInfo = (EarthQuakeEvent) getListAdapter().getItem(position);
        Log.d("ListView", "Event Latitude: " + mEarthQuakeEventInfo.getLatitude());
        Log.d("ListView", "event Longitude: " + mEarthQuakeEventInfo.getLongitude());



        AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
        alert.setTitle("Earthquake Data");

        mWebView = new WebView(this.getActivity());
        mWebView.loadUrl(mEarthQuakeEventInfo.getDetail());
        mWebView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){

                view.loadUrl(url);
                return true;
            }


        });

        alert.setView(mWebView);



        alert.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int whichButton){

            }

        });
        alert.show();


    }



    /*********************************************** INNER CLASS. ***********************************************/
    /*****
     * This is the AsyncTask to get the data that we need from the USGS....
     * Params    Progress,  PostExecute
     *
     *
     */

    private class GetGeoJsonData extends AsyncTask<String,     Integer,       ArrayList<EarthQuakeEvent> >{

        // String used for logging purposes...
        private static final String EarthQuakeDataHttp = "EarthQuakeDataHttp";
        private WeakReference<OstesListView> fragmentWeakRef;

        /**
         * Standard doInBackground used AsyncTask used to
         * get the GeoJson Data.
         * @param url
         * @return
         */

        @Override
        protected ArrayList<EarthQuakeEvent> doInBackground(String... url){

            ArrayList<EarthQuakeEvent> earthQuakeData;
            earthQuakeData =  this.getGeoJsonData(url[0].toString());
            return earthQuakeData;

        }


        /**
         * Method used to update the ListView.  This method
         * will populate the list with rows of the current seismic data.
         * @param result
         */
        @Override
        protected void onPostExecute(ArrayList<EarthQuakeEvent> result){



            OstesAdapter mEventAdapter = new OstesAdapter(OstesListView.this.getActivity(), R.layout.fragment_row_view, result);

            setListAdapter(mEventAdapter);

        }



        /***
         *
         * @param url of the datasource of the JSON object.
         *
         * @return A json object from the datasource.
         */
        public ArrayList<EarthQuakeEvent> getGeoJsonData(String url){


            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(url);
            String result = null;
            JSONObject mGeoJsonObject = null;
            ArrayList<EarthQuakeEvent> mEarthQuakeList = new ArrayList<EarthQuakeEvent>();
            HttpResponse response;

            try {
                response = mHttpClient.execute(mHttpGet);
                HttpEntity entity = response.getEntity();

                if(entity != null){

                    InputStream mInstream = entity.getContent();
                    result = this.convertStreamToString(mInstream);

                    // We have a JSONObject from the datasource.  We need to create the SeismicEvents
                    // based on the "type" of event.
                    mGeoJsonObject = new JSONObject(result);



                    // Instantiate an EarthQuakeEvent object....add it to the ArrayList.
                    // We then put the
                    for(int c = 0; c < mGeoJsonObject.getJSONArray("features").length(); c++) {
                        /******* This is here for development/debugging purposes.  Do not put into production *****/
                     /*   Log.d(EarthQuakeDataHttp, "Features from datafeed: " + mGeoJsonObject.getJSONArray("features").getString(c));
                        Log.d(EarthQuakeDataHttp, "Features type: " +          mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("properties").getString("type"));
                        Log.d(EarthQuakeDataHttp, "Feature Longitude: " +       mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(0));
                        Log.d(EarthQuakeDataHttp, "Feature Latitude: " +     mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(1)); */
                        // Build the Vector/HashMap of EarthQuake Objects...
                        // We then return this Vector/HashMap as a result and feed this to the onPostExecute method.

                        // TODO -- Fix the bug when we parse the data.  We are passing in the features and the same
                        //        Data keeps reappearing.  We are passing in the wrong data.
                        // Fixed -- on 08/23/2014.
                        mEarthQuakeList.add(EarthQuakeEvent.newInstance(mGeoJsonObject.getJSONArray("features").getJSONObject(c)));



                    }




                }



            } catch (Exception e) {
                Log.e(EarthQuakeDataHttp, "Error with http connection-> "+ e.getMessage() );
            }

            // Return the ArrayList...this goes to the postExecute method.
            return mEarthQuakeList;

        }


        /***
         * @param input stream, this comes in the form of a JSON input stream that needs to be converted to
         *              String Object.
         *  @return Returns a String object.  We can do what we need with this object.
         *
         */
        private String convertStreamToString(InputStream is) {
				    /*
				     * To convert the InputStream to String we use the BufferedReader.readLine()
				     * method. We iterate until the BufferedReader returns null which means
				     * there's no more data to read. Each line will be appended to a StringBuilder
				     * and returned as String.
				     */
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(EarthQuakeDataHttp, "Error while reading or building string: " + e.getMessage());
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }

    }



}

