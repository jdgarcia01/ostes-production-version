/**
 *  John Garcia
 *  EarthQuakeEvent class that handles
 *  modeling seismic events.
 *  08/21/2015
 *  Java Language
 */

package com.sealfit.android.geoquakemobile.app;

import java.util.UUID;
import org.json.JSONObject;

import android.location.Location;
import android.util.Log;



public class EarthQuakeEvent  {
    public static final String EARTHQUAKE = "EarthQuakeEvent";



    // Event ID assigned to the seismic event.
    private  UUID      mEventId;
    // String name assigned to the seismic event.
    private  String    mEventName;

    // double digit latitude for the event.
    private  double    mLatitude;

    // double digit longitude for the event.
    private  double    mLongitude;

    // Magnitude for the event.
    private  float     mEventMagnitude;


    private  int       mSig;  // significance of an event [0,1000] is the range.

    // time for the event, long that must be converted.
    private  long      mEventTime;
    private  String    mEventAlertColor;

    // event depth, in kilometers .
    private  float     mEventDepth;

    // Type of event, earthquake, volcanic, land slide etc.
    private  String    mEventType;

    private  JSONObject mJsonObject;
    private  String    mDetail;
    private static long mEventCount = 0;


    // newInstance method.  We use this technique because we are not
    // planning on sub-classing this class.

    /**
     * Called to create an instance of the object.
     * Don't call the default constructor.
     * @param json_object
     * @return
     */
    public static EarthQuakeEvent newInstance(JSONObject json_object ){

        return new EarthQuakeEvent(json_object);
    }

    // private constructor

    /**
     * Private constructor that should not be called.
     * @param json_object
     */
    private EarthQuakeEvent(JSONObject json_object){




        // Json array that we get from the USGS.  this object
        // is fed from the EarthQuakeData object.
        mJsonObject = json_object;


        try {

            this.setEventName(mJsonObject.getJSONObject("properties").getString("place"));
            this.setType( mJsonObject.getJSONObject("properties").getString("type") );
            this.setMagnitude( (float) mJsonObject.getJSONObject("properties").getDouble("mag") );
            this.setSig((int) mJsonObject.getJSONObject("properties").getDouble("sig")); // Significance of event.
            this.setDetail( mJsonObject.getJSONObject("properties").getString("url") );

            this.setAlert( mJsonObject.getJSONObject("properties").getString("alert") );
            this.mLatitude = mJsonObject.getJSONObject("geometry").getJSONArray("coordinates").getInt(1);
            this.mLongitude = mJsonObject.getJSONObject("geometry").getJSONArray("coordinates").getInt(0);
            this.setEventTime(mJsonObject.getJSONObject("properties").getLong("time"));    // Used to trend on time...


        } catch (Exception e){
            Log.e(EARTHQUAKE, "Error in JSON string, cannot get type");
            Log.e(EARTHQUAKE, "Error in JSON String: " + e.getCause());
        }


        this.setEventId( UUID.randomUUID());  // generate a unique UUID



    }


    // Typical Setters

    // This event id is the id that comes from the
    // GeoJson object from the USGS.
    // We do not use this id internally
    // for any


    public void setEventId(UUID id){

        if(id != null){
            mEventId = id;
            Log.d(EARTHQUAKE, "Random id for this event: " + id);
        } else {
            Log.e(EARTHQUAKE, "null id passed in from datasource. Earthquake event will not have an ID.");

        }

    }

    /**
     * A String title for the event.  Usually this
     * is just the location.
     * @param name
     */
    public  void setEventName(String name){

        if(name != null){
            mEventName = name;
        } else {
            Log.e(EARTHQUAKE, "Null passed in for event name, event will not have a name");

        }

    }


    /**
     * The latitude and longitude of the seismic event.
     * @param lat
     * @param lon
     */
    public void  setLocation(float lat, float lon ){

        mEventLocation.setLatitude(lat);
        mEventLocation.setLongitude(lon);


    }

    /**
     * The Magnitude of the event.
     * @param magnitude
     */
    public void setMagnitude( float magnitude){

        mEventMagnitude = magnitude;


    }

    /**
     * not used.  Here for future use.
     * @param sig
     */
    public void setSig(int sig){

        mSig = sig;

    }

    /**
     * The time of the event.
     * @param time
     */
    public void setEventTime(long time){

        mEventTime = time;




    }

    /**
     * The details of the event.
     * @param detail
     */
    public void setDetail(String detail){

        this.mDetail = detail;
    }
    public void setAlert(String alertColor){

        mEventAlertColor = alertColor;


    }

    /**
     * Set the Depth of the event.
     * @param depth
     */
    public void setDepth(float depth){

        mEventDepth = depth;



    }

    /**
     * Set the type of seismic event
     * @param type
     */

    public void setType(String type){

        mEventType = type;



    }

    /**
     * Returns the number of events that
     * have occurred.
     * @return
     */
    public static long getEventCount(){


        return mEventCount;

    }



    // Typical Getters

    /**
     * Returns the UUID or unique identifier
     * for the event.
     * @return
     */
    public UUID getEventId() {

        return mEventId;
    }

    /**
     * Returns the event name for the
     * specific event.
     * @return
     */
    public String getEventName() {

        return mEventName;
    }

    /**
     * Returns a location object (lat/lon)
     * for the event.
     * @return
     */
    public Location getLocation( ) {

        return mEventLocation;
    }

    /**
     * Returns a textual description
     * of the event.
     * @return
     */
    public String getDetail(){

        return mDetail;
    }

    /**
     * Returns the Magnitude for the
     * specific event.
     * @return
     */
    public float getMagnitude() {

        return mEventMagnitude;
    }


    /**
     * Returns the statistical significance of
     * the event.
     * @return
     */
    public float getSig(){

        return mSig;
    }

    /**
     * Returns the seismic event time.
     * @return
     */
    public long getEventTime() {

        return mEventTime;
    }

    /**
     * Returns alert information
     * @return
     */

    public String getAlert() {

        return mEventAlertColor;
    }

    /**
     * returns the depth of the seismic event.
     * @return
     */
    public float getDepth() {
        return mEventDepth;
    };

    /**
     * Returns the type of event (earthquake/tsunami/land slide etc.)
     * @return
     */
    public String getType(){

        return mEventType;

    }

    /**
     * Returns the latitude
     * @return
     */
    public double getLatitude(){
        return mLatitude;
    }


    /**
     * Returns the longitude of the event.
     * @return
     */
    public double getLongitude(){
        return mLongitude;
    }





}
