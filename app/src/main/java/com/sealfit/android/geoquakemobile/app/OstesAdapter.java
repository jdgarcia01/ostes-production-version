/**
 * John Garcia
 * OstesAdapter.java
 * 04/20/2015
 * This class is responsible for row formatting and
 * item selection on ListView.
 */

package com.sealfit.android.geoquakemobile.app;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.Date;

public class OstesAdapter extends ArrayAdapter<EarthQuakeEvent> {

    /**
     * String used in log records to identify the class.
     */
    private static final String TerraeMotusSeismicAdapter = "SeismicAdapter";

    /**
     * Android application context.  This is used internally by this
     * class hold the reference to the Android Context.
     */
    private  Context context;
    private  int layoutResourceId;
    private  ArrayList<EarthQuakeEvent> mData;

    /**
     * Public constructor.  Used to instantiate the
     * OstesAdapter.
     * @param context
     * @param row_view
     */
    public OstesAdapter(Context context, int row_view){
        super(context, row_view);
    }


    /**
     * Overloaded constructor to handle ArrayList of EarthQuakeEvents.
     * @param context
     * @param resource
     * @param data
     */
    public OstesAdapter(Context context, int resource, ArrayList<EarthQuakeEvent> data){
        super(context, resource, data);
        //super(context,  resource, data);
        this.context = context;
        /**
         * ID of the resource as defined in the string.xml document.
         * For a full description, please see the Android Documentation project.
         */
        this.layoutResourceId = resource;

        /**
         * Reference to the ArrayList of EarthQuakeEvents.
         */
        this.mData = data;


    }


    /**
     *  get the view, inflate and add the data/object info to each row.
     */

    @Override
    public View getView( int position, View convertView, ViewGroup parent){

        /**
         * Reference to the LayoutInflator, used to "explode" the interface XML
         * definitions that define the User interface design.
         */
        LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        /**
         * Default view.  We grab the UI defined by the XML.
         */
        View rowView = inflater.inflate(R.layout.fragment_row_view,parent, false);

        /**
         * Reference to all the widgets defined in the XML interface definition.
         */
        TextView mTextView_EarthQuakeTitle = (TextView)rowView.findViewById(R.id.map_textview);
        TextView mTextView_EarthQuakeMagnitude = (TextView)rowView.findViewById(R.id.textView2);
        TextView mTextView_EarthQuakeTime = (TextView)rowView.findViewById(R.id.textView3);

        /**
         * Grab all the data from the ArrayList objects via method accessors.
         * The data is used to populate the UI.
         */
        String type = mData.get(position).getType();
        float magnitude = mData.get(position).getMagnitude();
        String place = mData.get(position).getEventName();
        String color = mData.get(position).getAlert();
        long time    = mData.get(position).getEventTime();
        Date eventTime = new Date(time);


        /**
         * Set the location of the seismic event.
         */
        mTextView_EarthQuakeTitle.setText(place);

        /**
         * Set the Color code of the seismic event.  > 5.0 is red.
         * All others are a standard white.
         */
        if( magnitude > 5.0 ) {
            mTextView_EarthQuakeMagnitude.setTextColor(Color.RED);
        }


        /**
         * Convert the float to a string...We do this because
         * the setText method expects a string value.
         */
        mTextView_EarthQuakeMagnitude.setText(String.valueOf(magnitude));

        /**
         * Set the Event time on the TextView on the UI.
         */
        mTextView_EarthQuakeTime.setText(eventTime.toString());






        return rowView;


    }



}
