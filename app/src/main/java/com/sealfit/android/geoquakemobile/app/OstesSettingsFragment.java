/**
 * Created by John Garcia on 5/23/15.
 *
 * This class handles configuring and
 * saving user config data.
 * Language: Android/Java.
 *
 */


package com.sealfit.android.geoquakemobile.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;


public class OstesSettingsFragment extends Fragment {

    // defined for Log...
    private static final String SettingsFragment = "Terrae Motus Settings Fragment";

    /**
     * Reference to Android shared preferences
     * for Ostes.
     */
    private SharedPreferences mTerraePrefs;
    private SharedPreferences.Editor mPrefEditor;


    /**
     * Private constructor used keep users from
     * instantiating the class directly.  newInstance is
     * provided instead.
     */

    private OstesSettingsFragment(){


    }

    /**
     * used to control instantiation of
     * this class.
     * @return OstesSettingsFragment instance.
     */
    // newInstance used to create an instance of this fragment.
    public static OstesSettingsFragment newInstance(){

        return new OstesSettingsFragment();

    }

    /**
     * Android overridden onCreate method.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        // Open the Preferences.
        mTerraePrefs = 	this.getActivity().getSharedPreferences("com.example.sealfit.android.geoquakemobile.prefs", Context.MODE_PRIVATE);

    }

    /**
     * Android overridden onCreateView method.
     * @param inflater
     * @param view
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstanceState){



        View settings_fragment = inflater.inflate(R.layout.fragment_geo_quake_settings,view, false);

        RadioGroup mRadioGroup = (RadioGroup)settings_fragment.findViewById(R.id.radioGroup1);


        // Select the proper RadioButton based on the users preferences set, if not
        // select the second radio button (events for the past week).
        mRadioGroup.check(mTerraePrefs.getInt("RadioId", R.id.radio1 ));


        // Listen for Radio button changes, we save the changes in our local preferences.
        if(mRadioGroup != null){
            // Go into Edit Mode.

            mPrefEditor = mTerraePrefs.edit();

            mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    switch(checkedId) {

                        case R.id.radio0 :
                            // Pref info for events past hour.
                            mPrefEditor.putInt("RadioId", R.id.radio0);
                            mPrefEditor.putInt("RadioString", R.string.all_events_past_hour);
                            mPrefEditor.putInt("radio_string", R.string.past_hour_M2_5);
                          //  mPrefEditor.commit();
                            mPrefEditor.apply();
                            break;
                        case R.id.radio1:
                            // Pref info for events past day.
                            mPrefEditor.putInt("RadioId", R.id.radio1);
                            // mPrefEditor.putInt("RadioUrlName", R.string.all_events_past_day);
                            mPrefEditor.putInt("radio_string", R.string.past_day_M2_5);
                            mPrefEditor.apply();

                            break;

                        case R.id.radio2:
                            // Pref info for events past week
                            mPrefEditor.putInt("RadioId", R.id.radio2);
                            mPrefEditor.putInt("radio_string", R.string.past_7_days_M2_5);
                            mPrefEditor.apply();
                            break;

                        case R.id.radio3:
                            // Pref info for events past month.
                            mPrefEditor.putInt("RadioId", R.id.radio3);
                            mPrefEditor.putInt("radio_string", R.string.past_30_days_M2_5);
                            mPrefEditor.apply();

                            break;

                        case  R.id.radio4 :
                            // Pref info for significant events...
                            mPrefEditor.putInt("RadioId", R.id.radio4);
                            mPrefEditor.putInt("radio_string", R.string.past_30_days_significant);
                            mPrefEditor.apply();


                    }





                }
            });

        }

        return settings_fragment;


    }



}
