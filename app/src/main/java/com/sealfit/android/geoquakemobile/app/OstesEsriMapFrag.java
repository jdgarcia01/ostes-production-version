/**
 * Created by jgarcia on 8/20/15.
 * This ArcGIS Map Fragment is used to display app seismic events
 * for the last 90 days (Default Value).  The user
 * can also drag and draw an envelope on the screen.  This
 * envelope is used to select the seismic events that they
 * are interested in to perform additional statistics on.
 *
 * Language: Android/Java.
 *
 */



package com.sealfit.android.geoquakemobile.app;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.Callout;
import com.esri.android.map.FeatureLayer;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapOnTouchListener;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISDynamicMapServiceLayer;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.core.geodatabase.GeodatabaseFeatureServiceTable;
import com.esri.core.geodatabase.GeodatabaseFeatureTable;
import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.internal.catalog.FeatureCollection;
import com.esri.core.map.CallbackListener;
import com.esri.core.map.Feature;
import com.esri.core.map.FeatureResult;
import com.esri.core.map.Field;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.SimpleFillSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.tasks.SpatialRelationship;
import com.esri.core.tasks.ags.query.Query;
import com.esri.core.tasks.query.QueryParameters;
import com.sealfit.android.geoquakemobile.app.dummy.OstesSeismicEvent;

import org.apache.http.HttpEntity;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class OstesEsriMapFrag extends Fragment {


    /**
     * Variable to the main ESRI Map service
     */
    private ArcGISDynamicMapServiceLayer mEarthquakeServiceLayer = null;
    /**
     * Map service
     */
    private ArcGISDynamicMapServiceLayer mTectonicMapServiceLayer = null;

    /**
     * ESRI Feature layer variable used by
     */
    private FeatureLayer feature_layer;

    /**
     * ESRI GeoDatabase used to store feature data.
     */
    private GeodatabaseFeatureServiceTable mTable;

    /**
     * Main MapView variable for the ESRI map that
     * we display to the user.
     */
    private MapView mMapView;

    /**
     * reference to the ESRI graphics layer.
     */
    private GraphicsLayer graphicsLayer = null;

    /**
     *  Reference to the USGS base layer.
     */
    private GraphicsLayer mUSGSBaseLayer = null;


    /**
     *  Reference to the popup/callout text box.
     */
    private View callOutView = null;
    private View statisticalCallout = null;
    private Callout mapCallout = null;

    /**
     *  Android Log string used by the loggin mechanism.
     */
    private static final String OstesEsriMapFrag = "OstesEsriMapFrag";
    private static double current_mag = 0f;
    private GraphicsLayer gLayer = null;
    private SimpleFillSymbol sfs;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] mDrawerToolList  = {"Topo Layer", "Satellite Layer", "Street Layer"};
    private ActionBarDrawerToggle mDrawerToggle;

    private GeoJsonParser mGeoJsonParser;

    private DetermineSeismicStats mSeismicStats;
    private String file_path;
    private File mSeismicGeojson;



    /**
     * This is the constructor that a developer
     * should call.  The developer should call
     * the newInstance() method instead of
     * calling the default constructor.
     * */
    public static OstesEsriMapFrag newInstance(){

        return new OstesEsriMapFrag();


    }

    /**
     * This callback is the heart of our stats for seismic events.
     * This will be moved to an AsyncTask for execution on a separate
     * thread.
     */

    CallbackListener<FeatureResult> SeismicStats = new CallbackListener<FeatureResult>() {

        private double average = 0f;
        private double ave_depth = 0f;
        private long   divider = 0;
        private double depth;
        private double max_mag = 0f;
        private double max_depth = 0f;

        private Date   event_date;
        private String location;
        private double magnitude = 0.0;
        private ArrayList<Double> magnitude_list;
        private ArrayList<OstesSeismicEvent> mSeismicList;


        /**
         * Callback function that is called.  This method is the heart
         * of Ostes.  It calculates the average magnitude, depth, maximum and
         * minimum magnitude for events.
         * */
        public void onCallback(FeatureResult fSet) {

            /**
             * To calculate the average magnitude of n events
             * we just divide by the total number of events.
             */
            divider = fSet.featureCount();

            /**
             * Create a Double array to hold the
             * magnitude of each event.  Used to get the
             * average magnitude.
             */
            magnitude_list = new ArrayList<Double>();

            /**
             * This list contains a list of all the events.
             * We pass this to the actual Async Object below to
             * perform additional calculations.....
             */
            mSeismicList = new ArrayList<OstesSeismicEvent>();



           List<Field> list = fSet.getFields();


           for( Field f : list)
            Log.d(OstesEsriMapFrag, "list is: " + f.getName());


            /**
             *  This loop will iterate through the ESRI result set
             *  and create a new EarthQuake Object.  this Object is used
             *  later on to make operating on each event much easier.
             */
            for(Object element : fSet){

                if(element instanceof Feature){



                   Feature feature = (Feature) element;

                    magnitude  =  magnitude + (Double)feature.getAttributeValue("MAGNITUDE");
                    depth      = depth + (Double)feature.getAttributeValue("DEPTH");
                    event_date = new Date((Long)feature.getAttributeValue("UTC_DATETIME"));
                    location   = (String)feature.getAttributeValue("LOCATION");



                  magnitude_list.add((Double)feature.getAttributeValue("MAGNITUDE"));


                    Log.d(OstesEsriMapFrag, "Magnitude: " + feature.getAttributeValue("MAGNITUDE"));
                    Log.d(OstesEsriMapFrag, "Sum of the Magnitude: " + magnitude);
                    Log.d(OstesEsriMapFrag,"The depth is: " + depth);
                    Log.d(OstesEsriMapFrag, "The UTC Time is: " + feature.getAttributeValue("UTC_DATETIME"));
                    Log.d(OstesEsriMapFrag, "The date is: " + event_date.toString());
                    Log.d(OstesEsriMapFrag, "The Location is: " + location.toString());
                    Log.d(OstesEsriMapFrag, "The number of events is: " + divider);
                    Log.d(OstesEsriMapFrag, "The Attributes map is: " + feature.getAttributes().toString());

                    // Add each event to the list.  This list is used by our Asynctask.  We also
                    // use this list to create an IOStream just in case the user decides to email the
                    // results to another person.
                    mSeismicList.add( new OstesSeismicEvent(magnitude, depth, event_date, location));

                    // Get the current magnitude to store the value.
                    // We use it to determine the minimum value;
                    current_mag = (Double)feature.getAttributeValue("MAGNITUDE");

                    // compare it to the stored value.
                    if((Double)feature.getAttributeValue("MAGNITUDE") <= current_mag){
                       current_mag = 0f;
                        current_mag = (Double)feature.getAttributeValue("MAGNITUDE");

                        Log.d(OstesEsriMapFrag,"The current minimum is: " + current_mag);


                    }

                    // if the magnitude is greater than or equal
                    // to the current max_mag, then
                    // set max_mag = 0 and then take the

                    if( (Double)feature.getAttributeValue("MAGNITUDE") >= max_mag){
                        max_mag = 0f;
                        max_mag = (Double)feature.getAttributeValue("MAGNITUDE");
                    }

                    if((Double)feature.getAttributeValue("DEPTH") >= max_depth){

                        max_depth = 0;
                        max_depth = (Double)feature.getAttributeValue("DEPTH");

                    }



                }

            }

            // Get the maximum magnitude from all the events.

            average = magnitude / divider;
            ave_depth = depth / divider;

            Log.d(OstesEsriMapFrag, "The average magnitude is: "  + average);
            Log.d(OstesEsriMapFrag, "The average depth is: "      + ave_depth);
            Log.d(OstesEsriMapFrag, "The maximum magnitude is: "  + max_mag);
            Log.d(OstesEsriMapFrag, "The maximum depth is: "      + max_depth);



            // Pass int the seismic event arraylist.  This will hold the
            // the information that we have for each event.

            mSeismicStats = new DetermineSeismicStats( max_mag, ave_depth, average, max_depth, divider );

            //TODO -- change the AsyncTask to use the mSeismicList...
            mSeismicStats.execute(mSeismicList);



            updateStatisticalCallout(Double.toString(average), Double.toString(ave_depth));



            average = 0.0;
            magnitude = 0.0;
            max_mag = 0.0;
            max_depth = 0.0;
            ave_depth = 0.0;
            depth = 0.0;
            divider = 0;


        }

        public void onError(Throwable arg0) {
            gLayer.removeAll();
        }
    };


    /**
     *
     * @param savedInstance
     * onCreate method for the map fragment.
     */

    @Override
    public void onCreate(Bundle savedInstance){
      super.onCreate(savedInstance);

        /**
         * GraphicsLayer mUSGSBaseLayer
         * This is the base layer for the USGS data feed.
         * We use this GraphicsLayer to display the earthquakes
         * from the GEOJson data feed.
         *
         */

        mUSGSBaseLayer = new GraphicsLayer();



        // Create the two service layers here.  The first one displays the current earthquakes.
        // The second one displays the service for tectonic plates.
        mEarthquakeServiceLayer = new ArcGISDynamicMapServiceLayer(getString(R.string.earthquakes_current));
        mTectonicMapServiceLayer = new ArcGISDynamicMapServiceLayer(getString(R.string.tectonic_plate_map_service));



        // This is our callout.  The callout is the ArcGIS popup dialog.
        callOutView = View.inflate(getActivity(), R.xml.callout, null);
    //    statisticalCallout = View.inflate(getActivity(), R.xml.statistics_callout, null);


        graphicsLayer = new GraphicsLayer();



        // This code creates a GeoDatabase for the map service that holds the
        // 90 day data.  This is stored in an in-memory database.
        // This allows for faster queries etc.
        mTable = new GeodatabaseFeatureServiceTable(getString(R.string.earthquakes_current), 0);
        mTable.initialize(new CallbackListener<GeodatabaseFeatureServiceTable.Status>() {
            @Override
            public void onCallback(GeodatabaseFeatureServiceTable.Status status) {
                if(status == GeodatabaseFeatureServiceTable.Status.INITIALIZED){

                     feature_layer  = new FeatureLayer(mTable);
                     feature_layer.setSelectionColor(Color.MAGENTA);
                    mMapView.addLayer(feature_layer);
                    Toast.makeText(getActivity(), "Feature Added", Toast.LENGTH_SHORT).show();


                    mTable.setOutFields(new String[] {"*"});
                }
            }

            @Override
            public void onError(Throwable throwable) {

                Toast.makeText(getActivity(), "Error initializing table", Toast.LENGTH_SHORT).show();

            }
        });





        // Create the Feature Table.
        mGeoJsonParser = new GeoJsonParser();

       try {


           json_url = new URL(getString(R.string.past_30_days_significant));


       } catch( MalformedURLException e){

           Log.e(OstesEsriMapFrag, "The url is malformed..." + e.getMessage());

       }

        try {


              // Read the file from the file system.  We use the contents of this
              // file to create the feature layer for the system.
              file_path = this.getActivity().getFilesDir() +"/" + "seismic.geojson";
              mSeismicGeojson  = new File(file_path);

             Log.d(OstesEsriMapFrag, "file_path: " + file_path.toString());



        } catch (Exception e) {

            Log.e(OstesEsriMapFrag, "Illegal Argument: " + e.getMessage());


        }


            try {

                List<Feature> features = mGeoJsonParser.parseFeatures(mSeismicGeojson);


                // create the feature layer from the feature list.



            } catch (Exception e){

                Log.e(OstesEsriMapFrag, "An exception was thrown: " + e.getMessage());
                Log.e(OstesEsriMapFrag, "File exception stack trace: " + e.getStackTrace().toString());
            }




        /**
         * TEST AREA for LOADING THE GEOJSON CODE!!!!
         * This is not where we will put this.
         *
         */


        mDrawerLayout = (DrawerLayout) View.inflate(getActivity(), R.layout.ostes_esri_map_drawer, null);
        mDrawerList   = (ListView) mDrawerLayout.findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, mDrawerToolList));

        // Drawer toggle menu.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),
                mDrawerLayout,
                R.drawable.ic_launcher,
                R.string.drawer_open,
                R.string.drawer_close

        );


     mDrawerLayout.setDrawerListener(mDrawerToggle);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().getActionBar().setHomeButtonEnabled(true);



    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstance
     * @return A created View.  This is handled internally by the
     * Android device.
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance){

        View mEsriFragment = inflater.inflate(R.layout.fragment_esri_map_fragment, container, false);
        mMapView = (MapView)mEsriFragment.findViewById(R.id.esri_map);

        mMapView.setAllowRotationByPinch(true);
        mMapView.setEsriLogoVisible(true);
        mMapView.enableWrapAround(true);

        // This graphics layer is used to
        // handle drawing on the screen..
        mMapView.addLayer(graphicsLayer);

        // Add the USGS graphics layer.

        mMapView.addLayer(mUSGSBaseLayer);


       // mMapView.addLayer(mEarthquakeServiceLayer);
        mMapView.addLayer(mTectonicMapServiceLayer);

        mMapView.enableWrapAround(true);

        gLayer = new GraphicsLayer(GraphicsLayer.RenderingMode.DYNAMIC);
        sfs = new SimpleFillSymbol(Color.MAGENTA);
        sfs.setOutline(new SimpleLineSymbol(Color.RED, 2));
        sfs.setAlpha(100);



        mMapView.addLayer(gLayer);

      //  mMapView.setOnSingleTapListener(new OstesSingleTapListener());
        mMapView.setOnTouchListener(new OstesTouchListener(getActivity(), mMapView));

        return mEsriFragment;


    }





    // TODO -- Add the Envelope and Drag listener.

    /**
     * Inner class that handles user interactions, taps, drags etc.
     * Implements the ERSI OnSingleTapListener.
     */

    private class OstesSingleTapListener implements OnSingleTapListener {


        float mag;
        Date  event_date;


        @Override
        public void onSingleTap(float x, float y) {


            if (feature_layer.getFeatureIDs(x, y, 20).length > 0) {
                feature_layer.clearSelection();

                Point mMapPoint = mMapView.toMapPoint(x,y);

                long[] iDs = feature_layer.getFeatureIDs(x,y,20);

                feature_layer.selectFeature(iDs[0]);
                Feature f = feature_layer.getFeature(iDs[0]);


                Double magnitude =  (Double)f.getAttributeValue("MAGNITUDE");
                Double depth     =  (Double)f.getAttributeValue("DEPTH");
                String location  =  (String)f.getAttributeValue("LOCATION");
                event_date       =  new Date((Long)f.getAttributeValue("UTC_DATETIME"));


                updateContent(location,new Double( magnitude).toString(), new Double(depth).toString(), event_date.toString());

                mapCallout = mMapView.getCallout();

                mapCallout.setCoordinates(mMapPoint);
                mapCallout.setOffset(0,-3);
                mapCallout.setContent(callOutView);
                mapCallout.show();



            } else {

                /**
                 *
                 * If the callout window is showing then,
                 * dismiss/hide.
                 */
                if(mapCallout.isShowing())
                   mapCallout.hide();

            }

        }





    }


    /**
     * updateContent is responsible for updating the callout
     * window.  It grabs a reference to the callout windows
     * children widgets and does a setText to each child
     * widget.
     * @param mLocation
     * @param mMagnitude
     * @param mDepth
     */
private void updateContent(String mLocation, String mMagnitude, String mDepth,String mDate){

    if(callOutView == null){
        return;
    }




    // TODO--create a reference to the Textview for the location.
      TextView magnitude = (TextView) callOutView.findViewById(R.id.callout_magnitude);
       magnitude.setText("The Magnitude is: " +mMagnitude);

    //TODO -- create a reference to the TextView for the magnitude
      TextView location = (TextView) callOutView.findViewById(R.id.callout_location);

       location.setText("Location: " + mLocation);
    //TODO -- create a reference to the TextView for depth.

     TextView depth = (TextView) callOutView.findViewById(R.id.callout_depth);
    depth.setText(" Depth: " + mDepth + " KM");

    TextView dateTextView = (TextView)callOutView.findViewById(R.id.callout_date);
    dateTextView.setText("Time of event: " + mDate );





}

    /**
     * updateStatisticalCallout.
     * This method is used to update the contents
     * of the xml display used to return the results
     * of the statistical analysis.
     * @param average_mag
     * @param average_depth
     */

    private void updateStatisticalCallout( String average_mag, String average_depth){

        if(statisticalCallout == null)
            return;

        TextView ave_mag = (TextView) statisticalCallout.findViewById(R.id.statistics_callout_ave_magnitude);
        ave_mag.setText(average_mag);

        TextView ave_depth = (TextView) statisticalCallout.findViewById(R.id.statistics_callout_ave_depth);
        ave_depth.setText(average_depth);

        Button email_button = (Button) statisticalCallout.findViewById(R.id.statistics_callout_email_button);





    }


     class OstesTouchListener extends MapOnTouchListener {

        Graphic g;
        Point p0;
        int uid = -1;

        public OstesTouchListener(Context arg0, MapView arg1) {
            super(arg0, arg1);

        }

         public void onSingleTap(float x, float y) {


             if (feature_layer.getFeatureIDs(x, y, 20).length > 0) {
                 feature_layer.clearSelection();

                 Point mMapPoint = mMapView.toMapPoint(x,y);

                 long[] iDs = feature_layer.getFeatureIDs(x,y,20);

                 feature_layer.selectFeature(iDs[0]);
                 Feature f = feature_layer.getFeature(iDs[0]);


                 Double magnitude =  (Double)f.getAttributeValue("MAGNITUDE");
                 Double depth     =  (Double)f.getAttributeValue("DEPTH");
                 String location  =  (String)f.getAttributeValue("LOCATION");
                 Date   eventDate =  new Date((Long)f.getAttributeValue("UTC_DATETIME"));




                 updateContent(location,new Double( magnitude).toString(), new Double(depth).toString(), eventDate.toString());

                 mapCallout = mMapView.getCallout();

                 mapCallout.setCoordinates(mMapPoint);
                 mapCallout.setOffset(0,-3);
                 mapCallout.setContent(callOutView);
                 mapCallout.show();



             } else {

                 /**
                  *
                  * If the callout window is showing then,
                  * dismiss/hide.
                  */
                 if(mapCallout.isShowing())
                     mapCallout.hide();

             }

         }


         /**
          * onSingleTap method overridden
          * to handle ESRI single tap action.
          * @param event
          * @return
          */
         public boolean onSingleTap(MotionEvent event){


             mapCallout = mMapView.getCallout();

             if (feature_layer.getFeatureIDs(event.getX(), event.getY(), 20).length > 0) {
                 feature_layer.clearSelection();

                 Point mMapPoint = mMapView.toMapPoint(event.getX(), event.getY());

                 long[] iDs = feature_layer.getFeatureIDs(event.getX(),event.getY(),20);

                 feature_layer.selectFeature(iDs[0]);
                 Feature f = feature_layer.getFeature(iDs[0]);


                 Double magnitude =  (Double)f.getAttributeValue("MAGNITUDE");
                 Double depth     =  (Double)f.getAttributeValue("DEPTH");
                 String location  =  (String)f.getAttributeValue("LOCATION");
                 Date   eventDate =  new Date((Long)f.getAttributeValue("UTC_DATETIME"));


                 updateContent(location,new Double( magnitude).toString(), new Double(depth).toString(), eventDate.toString());


                 mapCallout.setCoordinates(mMapPoint);
                 mapCallout.setOffset(0,-3);
                 mapCallout.setContent(callOutView);
                 mapCallout.show();



             } else {

                 /**
                  *
                  * If the callout window is showing then,
                  * dismiss/hide.
                  */
                 if(mapCallout.isShowing() == true && mapCallout != null) {
                     mapCallout.hide();
                 }

             }

             return false;
         }


         /**
          * Main method to handle the double tap
          * and drag events.
          * @param from
          * @param to
          * @return
          */
          public boolean onDoubleTapDrag(MotionEvent from, MotionEvent to){

            if(uid == -1){

                g = new Graphic(null, sfs);
                p0 = mMapView.toMapPoint(from.getX(), to.getY());

                uid = gLayer.addGraphic(g);


            } else {

                Point p2 = mMapView.toMapPoint(new Point(to.getX(), to.getY() ));
                Envelope envelope = new Envelope();
                envelope.merge(p0);
                envelope.merge(p2);
                gLayer.updateGraphic(uid, envelope);
            }

            return true;
        }


         /**
          * Method to handle the on drag up
          * event of the double tap.
          * @param to
          * @return
          */
          public boolean onDoubleTapDragUp( MotionEvent to){

            if (uid != -1) {
                g = gLayer.getGraphic(uid);
                if (g!= null && g.getGeometry() != null) {
                    feature_layer.clearSelection();
                    Query q = new Query();
                    QueryParameters qp = new QueryParameters();


                    // optional
                    qp.setWhere("MAGNITUDE > 0");
                    qp.setReturnGeometry(true);
                    qp.setInSpatialReference(mMapView.getSpatialReference());
                    qp.setGeometry(g.getGeometry());
                    qp.setSpatialRelationship(SpatialRelationship.INTERSECTS);

                    feature_layer.selectFeatures(qp, FeatureLayer.SelectionMode.NEW, SeismicStats);


                }
                gLayer.removeAll();

            }

            p0 = null;
            // Resets it
            uid = -1;
            return true;




        }


    }


    /**
     * This method will return an instance of the currently instantiated map.
     * @return ArcGIS MapView Object.
     */
    public MapView getSeismicMap(){


        return mMapView;

    }

    // Create the AsyncTask
    // Async Process to get EarthQuakes.
    private class GetGeoJsonData extends AsyncTask<String,     Integer,       ArrayList<EarthQuakeEvent> > {

        // String used for logging purposes...
        private static final String OstesGetData = "Map Async Call...";
        private MapView map_ref;


        @Override
        protected ArrayList<EarthQuakeEvent> doInBackground(String... url){

            ArrayList<EarthQuakeEvent> earthQuakeData;
            earthQuakeData =  this.getGeoJsonData(url[0].toString());
            return earthQuakeData;

        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            map_ref = getSeismicMap();

        }


        // TODO -- modify this method to accept the Vector/HashMap of the EarthQuakes returned from the
        // TODO -- doInBackground method.
        @Override
        protected void onPostExecute(ArrayList<EarthQuakeEvent> result){

            Log.d(OstesGetData, "Result for map is: "+ result.size());

            map_ref = getSeismicMap();

        }



        /***
         * Inner class that is used to grab seismic data
         * f
         * @param url of the datasource of the JSON object.
         *
         * @return A json object from the datasource.
         */
        public ArrayList<EarthQuakeEvent> getGeoJsonData(String url){


            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(url);
            String result = null;
            JSONObject mGeoJsonObject = null;
            FeatureCollection mFc;

            int size;


            ArrayList<EarthQuakeEvent> mEarthQuakeList = new ArrayList<EarthQuakeEvent>();
            HttpResponse response;
            SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(Color.rgb(255,128,64), 15, SimpleMarkerSymbol.STYLE.CIRCLE);



            try {
                response = mHttpClient.execute(mHttpGet);
                HttpEntity entity = response.getEntity();

                if(entity != null){

                    InputStream mInstream = entity.getContent();
                    result = this.convertStreamToString(mInstream);


                    if(result.length() == 0){
                        Log.e(OstesGetData, "We seem to have a problem with the return results");
                    }

                    // We have a JSONObject from the datasource.  We need to create the SeismicEvents
                    // based on the "type" of event.
                    mGeoJsonObject = new JSONObject(result);

                    JSONArray feature_arr = mGeoJsonObject.getJSONArray("features");


                    if(feature_arr.length() == 0){
                        Log.e(OstesGetData, "We did not get any features back.");
                    }

                    // Instantiate an EarthQuakeEvent object....add it to the ArrayList.
                    // We then put the
                 //   for(int c = 0; c < mGeoJsonObject.getJSONArray("features").length(); c++) {
                    for(int i = 0; i < feature_arr.length(); i++){

                        Log.d(OstesGetData, "Getting Data!");

                        JSONObject obj_geometry = feature_arr.getJSONObject(i)
                                .getJSONObject("geometry");
                        double lon = Double.parseDouble(obj_geometry.getJSONArray("coordinates")
                                .get(0).toString());
                        double lat = Double.parseDouble(obj_geometry.getJSONArray("coordinates")
                                .get(1).toString());
                        Point point = (Point) GeometryEngine.project(
                                new Point(lon, lat), SpatialReference.create(4326),
                                mMapView.getSpatialReference());

                        JSONObject obj_properties = feature_arr.getJSONObject(i)
                                .getJSONObject("properties");
                        Map<String, Object> attr = new HashMap<String, Object>();

                        String place = obj_properties.getString("place").toString();
                        attr.put("Location", place);

                        // Setting the size of the symbol based upon the magnitude
                        float mag = Float.valueOf(obj_properties.getString("mag")
                                .toString());
                    //    size = getSizefromMag(mag);
                    //    symbol.setSize(size);
                        attr.put("Magnitude", mag);

                        // Converting time from unix time to date format
                        long timeStamp = Long.valueOf(obj_properties.getString("time")
                                .toString());
                        java.util.Date time = new java.util.Date((long) timeStamp);
                        attr.put("Time ", time.toString());

                        attr.put("Rms ", obj_properties.getString("rms").toString());
                        attr.put("Gap ", obj_properties.getString("gap").toString());

                        // Add graphics to the graphic layer
                        mUSGSBaseLayer.addGraphic(new Graphic(point, symbol, attr));


                    }




                }



            } catch (Exception e) {
                Log.e(OstesGetData, "Error with http connection-> "+ e.getMessage() );
            }

            // Return the ArrayList...this goes to the postExecute method.
            return mEarthQuakeList;

        }


        /***
         * @param input stream, this comes in the form of a JSON input stream that needs to be converted to
         *              String Object.
         *  @return Returns a String object.  We can do what we need with this object.
         *
         */
        private String convertStreamToString(InputStream is) {
				    /*
				     * To convert the InputStream to String we use the BufferedReader.readLine()
				     * method. We iterate until the BufferedReader returns null which means
				     * there's no more data to read. Each line will be appended to a StringBuilder
				     * and returned as String.
				     */
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(OstesGetData, "Error while reading or building string: " + e.getMessage());
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }

    }

    /**
     * Inner class that is used to calculate Seismic stats.  This executes on
     * a separate thread.
     */
    class DetermineSeismicStats extends AsyncTask< ArrayList<OstesSeismicEvent>, Void, Double >{

        double stats_sum = 0f;
        double average = 0.2f;
        double mAveragedepth = 0.2f;
        double mMaximumMag = 0.2f;
        double mMaxDepth   = 0.0f;
        long    mCount      = 0;

        /**
         * This constructor is used to pass in the maximum magnitude and
         * the average depth that will be displayed by this classes onPostExecute method.
         * These values are determined by the developer and passed in.
         * @param max_mag
         * @param ave_depth
         */
        public DetermineSeismicStats( double max_mag, double ave_depth, double ave, double max_depth, long count) {

            mAveragedepth = ave_depth;
            mMaximumMag   = max_mag;
            average       = ave;
            mMaxDepth     = max_depth;
            mCount        = count;



        }

        @Override
        protected void onPreExecute(){


         /*   new AlertDialog.Builder(getActivity()).
                    setTitle("Email? ").
                    setMessage("Would you like to email the results?")

            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Continue on.
                }
            }).setIcon(android.R.drawable.ic_dialog_info)
                    .show();  */



        }


        @Override

        protected Double doInBackground(ArrayList<OstesSeismicEvent>...list){



          if(list[0].size() != 0){

            for( int i = 0; i < list[0].size(); i++){

                // Get the magnitude of each index item and
                // add it up.


             //   stats_sum +=  list[0].get(i).getmMagnitude();

                // Determine the average from the size of the list and devide
                // by the size of the list the total of the magnitudes.

             //   average = stats_sum / list[0].size();

            }

          }

              return average;

        }

        @Override
        protected void onPostExecute(Double result_sum) {


            // format the maximum magnitude to two digits.
            DecimalFormat mMaxMagFormat = new DecimalFormat("#.##");


            new AlertDialog.Builder(getActivity())
                    .setTitle("Seismic Statistics")
                    .setMessage(getString(R.string.average_magnitude_message) +
                            " " + mMaxMagFormat.format(result_sum ) + " on the richter scale.\n\n\n" +
                     getString(R.string.average_depth_message) +
                            " " + mMaxMagFormat.format( mAveragedepth ) +
                            " KM \n\n\n " + " The maximum magnitude is: "
                            + mMaxMagFormat.format(mMaximumMag) + " on the richter scale.\n\n\n"
                            + "The maximim depth is: " + mMaxMagFormat.format(mMaxDepth) + " KM\n\n\n" +
                             " The number of seismic events for the past 90 days: " + mCount)


                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Continue on.
                        }
                    }).setIcon(android.R.drawable.ic_dialog_info)
                    .show();

                mAveragedepth = 0.0;

        }

    }

}
