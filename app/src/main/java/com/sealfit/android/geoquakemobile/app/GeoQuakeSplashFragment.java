/**
 * John Garcia
 * Class to display company Logo and splash screen
 * 04/21/2015
 * Java Language
 * */



package com.sealfit.android.geoquakemobile.app;

import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.os.Bundle;

/**
 * Created by jgarcia on 8/10/14.
 */
public class GeoQuakeSplashFragment extends Fragment {


    /**
     * Method used to create a private class.  This
     * allows instances to be created in a limited
     * and controlled manner.
     * @return
     */
    public static GeoQuakeSplashFragment getInstance() {

        return new GeoQuakeSplashFragment();

    }


    /**
     * Private constructor, cannot be called
     * explicitly.
     */
    private GeoQuakeSplashFragment(){
        // private constructor defined
        // use getInstance to return a new Fragment...
    }

    /**
     * Overridden Android method to create
     * User interface.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);


    }

    /**
     * Used by the Android VM to create the UI
     * of the application.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View mSplashScreenFragment = inflater.inflate(R.layout.fragment_splash_screen, container, false);
        return mSplashScreenFragment;

    }





}
