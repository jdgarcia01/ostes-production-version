/**
 *  John Garcia
 *  GeoQuakeMain
 *  modeling seismic events.
 *  08/21/2015
 *  Java Language
 *
 * This is the main activity that will be used to
 * host all the fragments of the earthquake monitor app.
 * It will host the main google maps as well
 * as other fragments.
 *
 */

package com.sealfit.android.geoquakemobile.app;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import com.google.android.gms.maps.*;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;






public class GeoQuakeMain extends ActionBarActivity  {

    // String used for Android logging.
    private static final String GEO_QUAKE = "GeoQuakeMainActivity";

    private static final String ACTIVITY_TAG = "activity_fragment";
   // Reference files used...
    private GetFileObjects mGetFileObjects;


   private static GoogleMap mMap;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        Log.d(GEO_QUAKE, "onCreate called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_quake_main);

        mGetFileObjects = new GetFileObjects(GeoQuakeMain.this);
        mGetFileObjects.execute();



        android.app.FragmentManager fm = this.getFragmentManager();
        mOstesListView = (OstesListView) fm.findFragmentByTag(ACTIVITY_TAG);


        if(savedInstanceState == null) {
            Fragment mOstesListView = GeoQuakeSplashFragment.getInstance();
            this.getFragmentManager().beginTransaction().replace(R.id.geo_quake_container, mOstesListView).commit();

        }
        Fragment mOstesListView = GeoQuakeSplashFragment.getInstance();
        this.getFragmentManager().beginTransaction().replace(R.id.geo_quake_container, mOstesListView).commit();


       // data.getTitle();
        Log.i(GEO_QUAKE,"Launching main" );

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){

        super.onSaveInstanceState(savedInstanceState);
        ActionBar mActionBar = this.getActionBar();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.geo_quake_main, menu);



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_seismic_events){

            // Display the sesimic events list.
            // this is a visual list.
           OstesListView mListView = OstesListView.newInstance();
           this.getFragmentManager().beginTransaction().replace(R.id.geo_quake_container,mListView).commit();
            return true;

        }


         if (id == R.id.action__settings_map) {


          // Start the Map FragmentActivity....
            Intent map_intent = new Intent(this.getApplicationContext(),GeoQuakeMapFragment.class);

              this.startActivity(map_intent);

             return true;
        }

        if( id == R.id.action_seismic_settings){

            OstesSettingsFragment mSettingsFragment = OstesSettingsFragment.newInstance();
            this.getFragmentManager().beginTransaction().replace(R.id.geo_quake_container, mSettingsFragment).commit();

            return true;

        }

        if(id == R.id.action_statistics)
        {
            OstesStatistcisFragment mStatisticsFragment = OstesStatistcisFragment.newInstance();
            this.getFragmentManager().beginTransaction().replace(R.id.geo_quake_container, mStatisticsFragment).commit();
            return true;
        }

        if( id == R.id.action_exit){

            Fragment mSplashScreen = GeoQuakeSplashFragment.getInstance();
            this.getFragmentManager().beginTransaction().replace(R.id.geo_quake_container, mSplashScreen).commit();

            Log.d(GEO_QUAKE, "Going Home");

        }

        if(id == R.id.action_ersi_map){

            OstesEsriMapFrag mEsriMapFrag = OstesEsriMapFrag.newInstance();

            this.getFragmentManager().beginTransaction().replace(R.id.geo_quake_container, mEsriMapFrag).commit();



        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *  Standard Android behavior methods.
     *  All overridden.
     */
    @Override
    public void onStart(){
        super.onStart();
        Log.d(GEO_QUAKE, "onStart");
    }

    @Override
    public void onResume(){
        super.onResume();
     Log.d(GEO_QUAKE, "onResume Called");
    }
    @Override
    public void onPause(){
        super.onPause();
        Log.d(GEO_QUAKE, "onPause");
    }
    @Override
    public void onStop(){
        super.onStop();
        Log.d(GEO_QUAKE, "onStop");

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(GEO_QUAKE, "onDestroy");

    }


    /**
     * Used to get grab USGS data for a pre-load
     * of data.  Hopefully this will speed up
     * the UI.
     */
    class GetFileObjects extends AsyncTask<Void, Void, Void> {

        public File seismic_file;
        public URL  mUsgsUrl;
        private ProgressDialog dialog;

        public GetFileObjects(GeoQuakeMain activity){

            dialog = new ProgressDialog(activity);
        }



        @Override
        protected void onPreExecute(){

           dialog.setMessage("Loading seismic data...please wait");
           dialog.show();

        }

        @Override
        protected void onPostExecute(Void result){

            if(dialog.isShowing()){
                dialog.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void...params){



            seismic_file = new File(getApplicationContext().getFilesDir() ,"seismic.geojson");

            try {
                mUsgsUrl = new URL("http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_month.geojson");


            } catch (MalformedURLException e){

                Log.e(GEO_QUAKE, "Malformed url exception: " + e.getMessage());

            }

            try {

                FileUtils.copyURLToFile(mUsgsUrl, seismic_file);

            } catch (IOException e){

                Log.e(GEO_QUAKE, "Exception occurred: " + e.getMessage());
            }


            return null;

        }

    }

}
